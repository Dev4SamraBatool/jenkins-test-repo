<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\Posts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php // $form->field($model, 'post_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'post_title')->dropDownList(
        yii\helpers\ArrayHelper:: map(app\models\UserLogin::find()->all(), "user_id", "username"),
            ["prompt" => "Select User"]
            ) ?>
    
    <?= $form->field($model, 'post_dec')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'author_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
