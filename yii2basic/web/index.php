<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

require(__DIR__ . '/../vendor/facebook/php-sdk/src/facebook.php');
$path = trim(__DIR__ ,"web");

//Fogbuz
require( dirname(__DIR__).'/vendor/fogbuzapi/src/There4/FogBugz/Api.php');
require( dirname(__DIR__).'/vendor/fogbuzapi/src/There4/FogBugz/Curl.php');
require( dirname(__DIR__).'/vendor/fogbuzapi/src/There4/FogBugz/ApiError.php');
require( dirname(__DIR__).'/vendor/fogbuzapi/src/There4/FogBugz/ApiConnectionError.php');
require( dirname(__DIR__).'/vendor/fogbuzapi/src/There4/FogBugz/ApiLogonError.php');
require( dirname(__DIR__).'/vendor/fogbuzapi/src/There4/FogBugz/CurlError.php');

//Trello
require( dirname(__DIR__).'/vendor/trelloapi/src/Trello/Trello.php');

//custom fogbuz api
require( dirname(__DIR__).'/vendor/customfogbuzapi/CustomFogbuzApi.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
