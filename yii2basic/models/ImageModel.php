<?php
namespace app\models;

use Yii;
use yii\base\Model;
use karpoff\icrop\CropImageUploadBehavior;
use yii\db\Query;

class ImageModel extends Model
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['photo', 'file', 'extensions' => 'jpeg, gif, png', 'on' => ['insert', 'update']],
        ];
    }

    /**
     * @inheritdoc
     */
    function behaviors()
    {
        return [
            [
                'class' => CropImageUploadBehavior::className(),
                'attribute' => 'photo',
                'scenarios' => ['insert', 'update'],
                'path' => '@webroot/upload/docs',
                'url' => '@web/upload/docs',
                'ratio' => 1,
                'crop_field' => 'photo_crop',
                'cropped_field' => 'photo_cropped',
            ],
        ];
    }
}