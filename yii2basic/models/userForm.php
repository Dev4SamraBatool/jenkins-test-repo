<?php

namespace app\models;

use yii\base\Model;

class UserForm extends Model{
    public $name;
    public $email;
    public $phoneno;
    
    public function rules(){
        return [
            [['name','email','phoneno'],'required'],
            ['email','email'],
        ];
    }
}

