<?php

namespace app\models;

use yii\base\Model;
use Trello\Trello;

class TrelloModel extends Model {

    public $trello;

    /* Subject : Login by using key and token of user
     * Name = index
     */

    public function index() {
        $this->trello = new Trello("a619489794272b0e1b75145896123b58", null, "1abc9dbf9a0cab11c2a0c5e98c62b6b097d608ef464071fb0a6107f2ad5ba308");
        return $this->trello;
    }

    /* Subject : Get Board ids of your account
     * Name = getBoard
     */

    public function getBoard() {
        $Getboard = $this->trello->boards->get("563ca0361dab794bf94b1a47", array('lists' => 'open', 'list_fields' => 'name'));
        return $Getboard;
    }

    /* Subject : get the details of user
     * Name = aboutMe
     */

    public function aboutMe() {
        $me = $this->trello->members->get("me");
        return $me;
    }

    /* Subject : Create board in account
     * Name = createBoard
     */

    public function createBoard() {
        $CreateBoard = $this->trello->post('boards', array('createBoard' => true,
            'name' => "Yii2 test board",
            'idlists' => 'listid',
            'lists' => 'open',
        ));
        return $CreateBoard;
    }

    /* Subject : delete board from your account
     * Name = deleteBoard
     */

    public function deleteBoard($BoardId) {
        $delBoard = $this->trello->boards->put($BoardId, array(
            'closed' => true,
        ));
        return $delBoard;
    }

    /* Subject : Add member to board in your account
     * Name = addMembersToBoard
     * Parameters = board id , member id required
     */

    public function addMembersToBoard() { //
        $Putboard = $this->trello->boards->put("56a867a242c6c64504960367" . "/members/" . "5637397d6ef7b03caac462f5", array(
            'scope' => array(
                'write' => true,
            ),
            "idMember" => "5637397d6ef7b03caac462f5",
            'fullName' => "mehwish monim",
            'username' => "mehwishmonim",
            'type' => 'normal',
        ));
        return $Putboard;
    }

    /* Subject : Create list to board in your account
     * Name = addMembersToBoard
     * Parameters = board id
     */

    public function createList() {
        $Createlist = $this->trello->post('lists', array(
            'createList' => true,
            'idBoard' => "56a867a242c6c64504960367",
            'name' => "Closed",
            'desc' => "This List contains closed cases/cards here.",
            'idlists' => 'listid',
            'pos' => 'bottom'
        ));
        return $Createlist;
    }

    /* Subject : Create list to board in your account
     * Name = getCardBy
     * Parameters = list id
     */

    public function getCardBy() {
        $getlist = $this->trello->lists->get("56a87043a01bc2b582cd9bad" . "/cards", array("card_fields" => 'all', "cards" => 'open'));
        return $getlist;
    }

    /* Subject : Create card to board in your account
     * Name = createCard
     * Parameters = list id (56a87043a01bc2b582cd9bad) , boardid(56a867a242c6c64504960367)
     */

    public function createCard() {
        $Createcard = $this->trello->post('cards', array(
            'createCard' => true,
            'idList' => "56a87043a01bc2b582cd9bad",
            'name' => "Yii2 case create as card1",
                //'due' => $CaseDueDate,
                //'idMembers' => array($MemberId, $EditorMemberId),
                //'idLabels' => $LabelidCat
        ));
        return $Createcard;
    }

    /* Subject : Update card to board in your account
     * Name = putCard
     * Parameters = list id (56a87043a01bc2b582cd9bad) , boardid(56a867a242c6c64504960367),cardid(56a87289c6ed749ce22ff782)
     */

    public function updateCard() {
        $cards = $this->trello->cards->put("56a87289c6ed749ce22ff782", array(
            'name' => "testing put card func",
            'idBoard' => "56a867a242c6c64504960367",
            'idList' => "56a87043a01bc2b582cd9bad",
        ));
        return $cards;
    }

    /* Subject : Delete card to board in your account
     * Name = deleteCard
     * Parameters = list id (56a87043a01bc2b582cd9bad) , boardid(56a867a242c6c64504960367),cardid(56a871999bb5da7de27afc65)
     */

    public function deleteCard() {
        $delCard = $this->trello->cards->delete("56a871999bb5da7de27afc65");
        return $delCard;
    }

    /* Subject : Create label to board in your account
     * Name = createLabel
     * Parameters = list id (56a87043a01bc2b582cd9bad) , boardid(56a867a242c6c64504960367),cardid(56a871999bb5da7de27afc65)
     */

    public function createLabel() {
        $Label = $this->trello->post('labels', array(
            'color' => "green",
            'idBoard' => "56a867a242c6c64504960367",
            'name' => "test Cat",
        ));
        return $Label;
    }

    /* Subject : Get label to board in your account, not working 
     * Name = getLabel
     * Parameters = list id (56a87043a01bc2b582cd9bad) , boardid(56a867a242c6c64504960367),cardid(56a871999bb5da7de27afc65), labelid(56a8733a607a994ad87ac4b8)
     */

    public function getLabel() {
        $Getlabels = $this->trello->labels->get("56a8733a607a994ad87ac4b8", array('fields' => 'color,name'));
        return $Getlabels;
    }
    
    /* Subject : Create Webhook to board in your account
     * Name = createBoardWebhook key token req ,webhook id got: 56a8961c328b1acc76874353
     * Parameters = list id (56a87043a01bc2b582cd9bad) , boardid(563ca0361dab794bf94b1a47),cardid(56a871999bb5da7de27afc65)
     */
    
    public function createBoardWebhook() {
        $webhook = array(
            'idModel' => "563ca0361dab794bf94b1a47",
            'callbackURL' => "http://bigmooselabs.com/projects/yii2basic/web/",
            'description' => "helllooo",
        );

        $post = $this->trello->post("tokens/1abc9dbf9a0cab11c2a0c5e98c62b6b097d608ef464071fb0a6107f2ad5ba308/webhooks/?key=a619489794272b0e1b75145896123b58", array_filter($webhook));
        return $post;
    }

}
