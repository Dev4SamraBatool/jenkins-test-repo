<?php

namespace app\models;

use yii\base\Model;
use There4\FogBugz\Api;
use customfogbuzapi\CustomFogbuzApi;

class FogbuzModel extends Model {

    public $fogbuz;
    public $customFgbLib;

    public function index() {
        $this->fogbuz = new Api(
                'dev4@bawanymedia.com', 'samdev4', 'https://codup.fogbugz.com/'
        );
        return $this->fogbuz->logon();
    }

    public function startWork() {
        $workingOn = $this->fogbuz->startWork(array(
            'ixBug' => 3604
        ));
        return $workingOn;
    }

    public function stopWork() {
        $workingOff = $this->fogbuz->stopWork(array(
            'ixBug' => 3604
        ));
        return $workingOff;
    }

    public function createCase() {
        $createCase = $this->fogbuz->new(array(
            'sTitle' => "test jenkins",
            'ixProject ' => 22,
            'ixFixFor' => 100,
            'ixCategory' => 3,
            'ixPersonAssignedTo' => 10));
        return $createCase->case['ixBug'];  //return id/sno of bug or case
    }

    public function editCase() {
        $EditCase = $this->fogbuz->edit(array(
            'ixBug' => 3604,
            'sTitle' => "yii case 3604s",
            'ixFixFor' => 100,
            'ixCategory' => 3,
            'ixPriority' => "",
            'ixPersonAssignedTo' => 10,
            'sStatus' => "Active",
            'sEvent' => "Helloooo descriptionnnnnn",
            'dtDue' => "",
        ));
        return $EditCase;
    }

    public function getListIntervals() {
        $listInterval = $this->fogbuz->listIntervals();
        return $listInterval;
    }

    public function caseClose($CaseNum) {
        $CloseCase = $this->fogbuz->close(array(
            'ixBug' => $CaseNum,
            'fRecurseBugChildren' => 0
        ));
        return $CloseCase;
    }

    public function reOpenCase($CaseNum) {
        $ReOpenCase = $this->fogbuz->reopen(array(
            'ixBug' => $CaseNum
        ));
        return $ReOpenCase;
    }

    public function viewFixForDetail($mileId) {
        $viewMileInfo = $this->fogbuz->viewFixFor(
                array(
                    'ixFixFor' => $mileId,
        ));
        return $viewMileInfo;
    }

    public function viewProjectDetail($projectId) {
        $viewProjectInfo = $this->fogbuz->viewProject(
                array(
                    'ixProject' => $projectId,
        ));
        return $viewProjectInfo;
    }

    public function viewListProject() {
        $viewProjectList = $this->fogbuz->listProjects();
        return $viewProjectList;
    }

    public function viewListMilestoneby($projectId) {
        $viewMileList = $this->fogbuz->listFixFors(array(
            'ixProject' => $projectId,
        ));
        return $viewMileList;
    }

    public function fogbuzCurlHit($params) {

        if (isset($params)) {
//            $paramsUrlFormat = http_build_query($params);
//            $c = str_replace("+", "%20", $paramsUrlFormat);            

            $url = 'https://codup.fogbugz.com/api.asp?';
            $ch = curl_init();

            if ($ch === false) {
                die('Failed to create curl object');
            }

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);

            // The submitted form data, encoded as query-string-style name-value pairs
            //$post_data = 'cmd=new&sTitle=test excel case mine yy 12th&sProject=trello scrum boards&sFixFor=test check si&sCategory=Feature&ixBugParent=3254&sPersonAssignedTo=Syeda Samra&token=h2m17m5r59og0fd7ahir291dplhkr3';
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $output = curl_exec($ch);

            curl_close($ch);
            $xml = simplexml_load_string($output) or die("Error: Cannot create object");

            return $xml->case['ixBug'];
        } else {
            return "Error: Cannot create object";
        }
    }

    public function curlRequestToFgb($token) {
        $customFgbLib = new \CustomFogbuzApi();
        // return $customFgbLib->createCase("jenkins Test case create", 22, 100, 2, 10, $token);
        return $customFgbLib->viewProjectDetail(22, $token);
    }

}
