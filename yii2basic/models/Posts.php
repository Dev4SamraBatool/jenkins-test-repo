<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Posts".
 *
 * @property integer $postId
 * @property string $post_title
 * @property string $post_dec
 * @property integer $author_id
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_title', 'post_dec', 'author_id'], 'required'],
            [['post_dec'], 'string'],
            [['author_id'], 'integer'],
            [['post_title'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'postId' => 'Post ID',
            'post_title' => 'Post Title',
            'post_dec' => 'Post Dec',
            'author_id' => 'Author ID',
        ];
    }
}
