<?php

class CustomFogbuzApi {

    public function fogbuzCurlHit($params) {

        $url = 'https://codup.fogbugz.com/api.asp?';

        $ch = curl_init();

        if ($ch === false) {
            die('Failed to create curl object');
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $output = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($output) or die("Error: Cannot create object");

        return $xml;
    }

    public function logon($email, $password) {
        $params = array(
            'cmd' => "logon",
            'email' => $email,
            'password' => $password);
        return $this->fogbuzCurlHit($params);
    }

    public function createCase($title, $projectId, $milestoneId, $categoryId, $assighnyId, $token) {
        $params = array(
            'cmd' => "new",
            'sTitle' => $title,
            'ixProject' => $projectId,
            'ixFixFor' => $milestoneId,
            'ixCategory' => $categoryId,
            'ixPersonAssignedTo' => $assighnyId,
            'token' => $token);
        return $this->fogbuzCurlHit($params);
    }

    public function editCase($caseNumber, $title, $milestoneId, $categoryId, $assighnyId, $status, $description, $dueDate, $token) {
        $params = array(
            'cmd' => "edit",
            'ixBug' => $caseNumber,
            'sTitle' => $title,
            'ixFixFor' => $milestoneId,
            'ixCategory' => $categoryId,
            'ixPersonAssignedTo' => $assighnyId,
            'sStatus' => $status,
            'sEvent' => $description,
            'dtDue' => $dueDate,
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function closeCase($CaseNum, $token) {
        $params = array(
            'cmd' => "close",
            'ixBug' => $CaseNum,
            'fRecurseBugChildren' => 0,
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function reopenCase($CaseNum, $token) {
        $params = array(
            'cmd' => 'reopen',
            'ixBug' => $CaseNum,
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function startWork($CaseNum, $token) {
        $params = array(
            'cmd' => 'startWork',
            'ixBug' => $CaseNum,
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function stopWork($CaseNum, $token) {
        $params = array(
            'cmd' => 'stopWork',
            'ixBug' => $CaseNum,
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function getListIntervals($token) {
        $params = array(
            'cmd' => 'listIntervals',
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function viewFixForDetail($mileId, $token) {
        $params = array(
            'cmd' => 'viewFixFor',
            'ixFixFor' => $mileId,
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function viewProjectDetail($projectId, $token) {
        $params = array(
            'cmd' => 'viewProject',
            'ixProject' => $projectId,
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function viewListProject($token) {
        $params = array(
            'cmd' => 'listProjects',
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

    public function viewListMilestoneby($projectId, $token) {
        $params = array(
            'cmd' => 'listFixFors',
            'ixProject' => $projectId,
            'token' => $token,
        );
        return $this->fogbuzCurlHit($params);
    }

}
