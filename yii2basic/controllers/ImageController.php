<?php

namespace app\controllers;

use Core\View;
use yii\web\Controller;
use app\models\ImageModel;

class ImageController extends Controller {

    public function actionIndex() {
        $usersModel = new ImageModel();
//        echo '<pre>';
//        print_r($usersModel);
        $this->render('image');
    }

}
